var showMoreInfoBtn = $('.showMoreInfo button'),
	showMoreInfoContent = $('.showMoreInfo p');

showMoreInfoBtn.click(function(){
	showMoreInfoContent.slideToggle();
});

var categories = $('.category');

categories.click(function(){
	categories.removeClass('active');
	$(this).addClass('active');
});
