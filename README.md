# theWorkshop - test by Omar Mirza

This project is generated with [yo generator-webapp](https://github.com/yeoman/generator-webapp)

## Prerequisites
1. Install node js `https://nodejs.org/en/download/`
2. Install dependencies `npm install --global yo gulp-cli bower`

## Build & development
1. Clone git repo `git clone https://OmarCreativeDev@bitbucket.org/OmarCreativeDev/theworkshop-test.git theworkshop-test-By-OmarMirza`
2. Navigate to repo `cd theworkshop-test-By-OmarMirza`
3. run `npm install`
4. run `bower install`
5. Run `gulp serve` to preview
